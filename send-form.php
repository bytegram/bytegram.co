<?php
use Psr\Http\Message\ServerRequestInterface;
use Google\Cloud\RecaptchaEnterprise\V1\RecaptchaEnterpriseServiceClient;
use Google\Cloud\RecaptchaEnterprise\V1\Event;
use Google\Cloud\RecaptchaEnterprise\V1\Assessment;
use Google\Cloud\RecaptchaEnterprise\V1\TokenProperties\InvalidReason;
use GuzzleHttp\Psr7\Response;

class Logger {

    private $log;

    function __construct() {
        $this->log = fopen("php://stderr", "w");
    }

    function info(String $message) {
        $this->writeLog($message, "info");
    }

    private function writeLog(String $message, String $severity = "info") {
        fwrite($this->log, json_encode([
            "message" => $message,
            "severity" => $severity
        ]));
    }

    function error(String $message) {
        $this->writeLog($message, "error");
    }

    function warning(String $message) {
        $this->writeLog($message, "warning");
    }

    function debug(String $message) {
        $this->writeLog($message, "debug");
    }
}

function send_mail($data) {
    $headers  = "Reply-To: " . $data['reply_to'] . PHP_EOL;
    $headers .= "Return-Path: " . $data['reply_to'] . PHP_EOL;
    $headers .= "From: forms@forms.bytegram.co" . PHP_EOL;
    $headers .= "Organization: Bytegram, LLC" . PHP_EOL;
    $headers .= "Mime-Version: 1.0" . PHP_EOL;
    $headers .= "Content-Type: text/plain; charset=utf-8" . PHP_EOL;
    $headers .= "X-Priority: 3" . PHP_EOL;
    $headers .= "X-Mailer: PHP " . phpversion() . PHP_EOL;

    $message  = "Dear Bytegram," . PHP_EOL . PHP_EOL;
    
    $message .= "My name is " . $data['from_name'] . " and I am interested in " . $data['project_type'] . "." . PHP_EOL . PHP_EOL;

    $message .= $data['message'] . PHP_EOL . PHP_EOL;

    $message .= "Sincerely," . PHP_EOL;
    $message .= "  " . $data['from_name'];

    if (!mail("seal@bytegram.co", $data['project_type'], $message, $headers)) {
        $last_error = error_get_last();
        throw new Exception($last_error['message'], $last_error['code']);
    }
}

function create_json_response($data, $httpCode = 200) {
    return new Response(
        $httpCode,
        ['Content-Type' => 'application/json'],
        $data
    );
}

function send_form(ServerRequestInterface $request)
{
    $logger = new Logger();

    $body = $request->getBody();
    $body->rewind();
    $data = parse_str($body->getContents());
    
    $client = new RecaptchaEnterpriseServiceClient();
    $projectName = $client->projectName("bytegramweb");

    $event = (new Event())
        ->setSiteKey("6Les0X0cAAAAAEyAU7s2MysVyfRBe2viYi3pbqZy")
        ->setToken($data['g-recaptcha']);

    $assessment = (new Assessment())
      ->setEvent($event);

    try {
        $response = $client->createAssessment(
            $projectName,
            $assessment,
        );

        if ($response->getTokenProperties()->getValid() == false) {
            $logger->error(InvalidReason::name($response->getTokenProperties()->getInvalidReason) . PHP_EOL);
            return create_json_response([
                "success" => false,
                "reason" => "Invalid Recaptcha"
            ], 400);
        } else {
            try {
                send_mail($data);
                return create_json_response(["success" => true]);
            } catch (Exception $e) {
                $logger->error("Unable to send message: " . $e->getMessage() . PHP_EOL);
                return create_json_response([
                    "success" => false,
                    "reason" => "The server was unable to forward the message"
                ], 500);
            }
        }
    } catch (Exception $e) {
        $logger->error("Unable to create assesment: " . $e->getMessage() . PHP_EOL);
        return create_json_response([
            "success" => false,
            "reason" => "The server was unable to verify the response"
        ], 500);
    }
}
