
const fs = require('fs');

const colorMap = JSON.parse(fs.readFileSync('/home/seal/Projects/Stratus/Stratus/src/components/GlobeComponents/countryColors.json'));
let countryColors = JSON.parse(fs.readFileSync('/home/seal/Projects/Bytegram Website/bytegram.co/public/countryColors.json'));

const colorFor = (rank, theme = 'day', muted = false, divisor = 5) => {
    if (rank === null) return '#cccccc';

  muted = muted === true ? 'muted' : 'full';

  const rankIdx = Math.floor((rank - 1) / divisor);
  // If we happen to get a number higher than the highest available rank,
  // just use the top rank
  if (rankIdx >= colorMap.length) rankIdx = colorMap.length - 1

  return  colorMap[rankIdx][theme][muted].hex;
}

const countryColorsData = countryColors.reduce((colors, { rank, isoA3 }) => {
    colors[isoA3] = colorFor(rank);
    return colors;
}, {});

fs.writeFileSync('/home/seal/Projects/Bytegram Website/bytegram.co/public/countryColorsData.json', JSON.stringify(countryColorsData));
