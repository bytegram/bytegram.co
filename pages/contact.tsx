import type { ReactElement } from 'react';
import Head from 'next/head';
import type { NextPageWithLayout } from './_app'
import Layout from '../components/layout';
import PageLayout from '../components/PageLayout';
import emailjs from 'emailjs-com'
import ContactForm from '../components/ContactForm';

const About: NextPageWithLayout = () => {
    return (
        <>
            <Head>
                <title>bytegram - Contact</title>
                <meta name="description" content="Contact Bytegram, LLC" />
                <link rel="icon" href="/favicon.png" />
            </Head>

            <h1>Contact</h1>
            <p>
                Contact Bytegram to start the conversation about the needs of your software solutions.
            </p>

            <ContactForm />
        </>
    );
}

About.getLayout = (page: ReactElement) => (
    <Layout>
        <PageLayout>{page}</PageLayout>
    </Layout>
);

export default About
