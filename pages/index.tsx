import type { ReactElement } from 'react'
import type { NextPageWithLayout } from './_app'
import Head from 'next/head'
import Link from 'next/link'
import BytegramBanner from '../public/bytegram-banner.svg'
import Layout from '../components/layout'
import styles from '../styles/Home.module.css'
import LandingLayout from '../components/LandingLayout'

const Home: NextPageWithLayout = () => {
  return (
    <>
      <Head>
        <title>bytegram</title>
        <meta name="description" content="Measured. Precise. Custom software." />
        <link rel="icon" href="/favicon.png" />
      </Head>

      <BytegramBanner alt="bytegram" width={742} height={202} />
      <p className={styles.description}>
        Measured. Precise. Custom software.
      </p>
        

      <div className={styles.grid}>
        <Link href="/about">
          <a className={styles.card}>
            <h2>About</h2>
            <p>Philosophy and offered services at Bytegram.</p>
          </a>
        </Link>

        <Link href="/portfolio">
          <a className={styles.card}>
            <h2>Portfolio</h2>
            <p>Projects bytegram contributed to.</p>
          </a>
        </Link>

        <Link href="/contact">
          <a className={styles.card}>
            <h2>Contact</h2>
            <p>
              Ready to start exploring if bytegram can help?
            </p>
          </a>
        </Link>

        <Link href="/labs">
          <a className={styles.card}>
            <h2>Labs</h2>
            <p>
              What&apos;s cooking and could be coming soon.
            </p>
          </a>
        </Link>
      </div>
    </>
  )
}

Home.getLayout = (page: ReactElement) => (
  <Layout>
    <LandingLayout>{page}</LandingLayout>
  </Layout>
);

export default Home
