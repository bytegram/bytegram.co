import { useState, useEffect, useCallback, useRef, forwardRef, MutableRefObject, ForwardedRef } from 'react'
import type { ReactElement } from 'react'
import dynamic from 'next/dynamic'
import Layout from '../components/layout'
import PageLayout from '../components/PageLayout'
import type { NextPageWithLayout } from './_app'
import { MeshPhongMaterial, Color } from 'three'
import styles from '../styles/Portfolio.module.css'
import type { GlobeMethods, GlobeProps } from 'react-globe.gl'

const GlobeTmp = dynamic(import('../components/Globe'), { ssr: false });
const Globe = forwardRef((props: GlobeProps, ref: ForwardedRef<GlobeMethods | undefined> | undefined) => {
    return <GlobeTmp xref={ref as any} {...props} />
});
Globe.displayName = 'Globe';

const globeTexture = new MeshPhongMaterial({ color: new Color('rgb(179, 217, 243)') });

const Portfolio: NextPageWithLayout = () => {
    const [polygonData, setPolygonData] = useState({ features: [] });
    useEffect(() => {
        fetch('/geojsonMinify.json')
            .then(res => res.json())
            .then(data => setPolygonData(data));
    }, []);

    const [colorData, setColorData] = useState<{ [key: string]: string }>({});
    useEffect(() => {
        fetch('/countryColorsData.json')
            .then(res => res.json())
            .then(data => setColorData(data));
    }, []);

    const globe = useRef<GlobeMethods | undefined>();
    useEffect(() => {
        if (globe?.current?.controls) {
            const controls: any = globe.current.controls();
            controls.autoRotate = true;
            controls.autoRotateSpeed = 0.7;
        }
    }, [globe.current?.controls]);

    const getColorData = useCallback((feature) => {
        return colorData[feature.properties.iso_a3] || '#cccccc';
    }, [colorData]);

    return (
        <>
            <h1>Portfolio</h1>

            <div className={styles.project}>
                <div className={styles.thumbnail}>
                    <Globe
                        ref={globe}
                        animateIn
                        width={300}
                        height={300}
                        backgroundColor="#ffff"
                        globeMaterial={globeTexture}
                        polygonsData={polygonData.features}
                        polygonCapColor={getColorData}
                    />
                </div>
                <a href='https://globe.stratus.earth'>
                    <div className={styles.details}>
                        <h2>Stratus</h2>
                        <p>
                            As part of a multi-organization project, Bytegram helped build and deploy a free-to-use
                            world data visualization tool.  The data informs users of countries current political,
                            social, natural and spiritual risks and needs.  User can interact with the globe and dive
                            deep into country-specific statistics.
                        </p>
                    </div>
                </a>
            </div>
        </>
    )
}

Portfolio.getLayout = (page: ReactElement) => (
    <Layout>
        <PageLayout>{page}</PageLayout>
    </Layout>
);

export default Portfolio;
