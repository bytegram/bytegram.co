import type { ReactElement } from 'react';
import Head from 'next/head';
import type { NextPageWithLayout } from './_app'
import Layout from '../components/layout';
import PageLayout from '../components/PageLayout';
import Card from '../components/Card'

const About: NextPageWithLayout = () => {
    return (
        <>
            <Head>
                <title>bytegram - About</title>
                <meta name="description" content="Measured. Precise. Custom software." />
                <link rel="icon" href="/favicon.png" />
            </Head>

            <h1>About Bytegram</h1>
            <p>
                Bytegram is a single member LLC established in 2021 by Ryan Enoch Seal.
                Ryan has been writing software professionally since 2008 with experience in
                full stack web development, desktop software, equipment automation and Linux
                system adminstration.
            </p>

            <Card title="Development Philosophy">
                <p>
                    Writing software is a complex process that can go awry without careful attention
                    to detail.  Furthermore, the wrong team or wrong developer can derail your product
                    when they misunderstand your intentions or vision.  Bytegram keeps development on
                    track and on mission with these simple principles:
                </p>

                <h3>Communicate</h3>
                <p>
                    Never assume the first understanding is correct.  Always verify before writing code.
                    Take time to design the software before writing it to catch design challenges or flaws
                    as early as possible
                </p>

                <h3>Measure what matters</h3>
                <p>
                    To many metrics are noisy an unhelpful, but too few leave you without a way to understand
                    your product or where it could improve.  Make sure the customer knows what is important to
                    track and track it from day one.
                </p>

                <h3>Keep it clean (maintainable)</h3>
                <p>
                    A cluttered house can become impossible to navigate, live in or keep safe.  While cleanliness
                    is only half of the battle, its the one thing that makes the other battles manageable. Code is
                    the same.  When bytegram passes off a project, the code is clean and ready for easy maintenance
                    by any developer.
                </p>
            </Card>

            <Card title="Testing">
                <p>
                    One of the most difficult justifications an organization can make when paying for
                    software is justifying paying for tests.  Mostly this is because the default strategy
                    for testing relies on what software engineers know as the testing trophy.  Following
                    that strategy often leads to high costs the generate a lot of unhelpful tests. Instead,
                    Bytegram follows the <a href="https://kentcdodds.com/blog/write-tests">Testing trophy</a> originally
                    championed by Kent C. Dodds.  It focuses developer efforts on tests
                    that give the customer the most confidence in the software without overcharging for a lot
                    of unused time.
                </p>
            </Card>

            <Card title="Mentoring and Training">
                <p>
                    Software development doesn&apos;t stop with the first release, the hand-off to operations or when
                    the lead developer leaves the project.  New features are requested, security holes are found
                    and dependencies are updated.  Sometimes you need a developer to kick off your product, but
                    sometimes you need something longer lasting.  Let bytegram train staff in new tech and
                    mentor your junior developers to help them reach the next level faster.
                </p>
            </Card>
        </>
    );
}

About.getLayout = (page: ReactElement) => (
    <Layout>
        <PageLayout>{page}</PageLayout>
    </Layout>
);

export default About
