import type { ReactElement } from 'react';
import { useState } from 'react';
import Head from 'next/head';
import type { NextPageWithLayout } from './_app'
import Layout from '../components/layout';
import PageLayout from '../components/PageLayout';
import Modal from '../components/Modal';
import ContactForm from '../components/ContactForm';
import Card from '../components/Card'
import styles from '../styles/Portfolio.module.css'

const projectsFormInfo: { [key: string]: { name: string, description: string } } = {
    "invoices": {
        "name": "Invoices for Freelancers App",
        "description": "The App is in alpha stage right now.  Send a message you'll be added to the early access wait list."
    },
    "optimizingReact": {
        "name": "Optimizing React Book",
        "description": "The book is still being written.  Send a message and you'll be added to receive early access."
    }
}

const Labs: NextPageWithLayout = () => {
    const [projectSelected, setProjectSelected] = useState("");

    return (
        <>
            <Head>
                <title>bytegram - Labs</title>
                <meta name="description" content="Measured. Precise. Custom software." />
                <link rel="icon" href="/favicon.png" />
            </Head>

            <h1>Bytegram Labs</h1>
            <p>
                There is always something new to learn in the world of software development and
                you don&quot;t learn unless you try it out.  Take a look at what Bytegram is experimenting
                with.
            </p>

            <a onClick={() => setProjectSelected("invoices")} className={styles.project}>
                <div className={styles.thumbnail}>
                    <svg viewBox="0 0 100 100">
                        <path
                            className={styles.invoiceIcon}
                            id="page-border"
                            d="M 20 5
                               L 80 5
                               Q 85 5 85 10
                               L 85 90
                               Q 85 95 80 95
                               L 20 95
                               Q 15 95 15 90
                               L 15 10
                               Q 15 5 20 5"
                        />
                        <rect className={styles.invoiceLineOdd} x="20" y="15" width="60" height="10" />
                        <rect className={styles.invoiceLineEven} x="20" y="30" width="60" height="10" />
                        <rect className={styles.invoiceLineOdd} x="20" y="45" width="60" height="10" />
                        <rect className={styles.invoiceLineEven} x="20" y="60" width="60" height="10" />
                        <rect className={styles.invoicePayment} x="50" y="75" width="30" height="10" />
                        <text className={styles.invoicePayment} x="55" y="83">$</text>
                        <g className={styles.paymentLanding}>
                            <path d="M 51 76 54 79" />
                            <path d="M 51 80 54 80" />
                            <path d="M 51 84 54 81" />
                            <path d="M 61 79 64 76" />
                            <path d="M 61 80 64 80" />
                            <path d="M 64 84 61 81" />
                        </g>
                    </svg>
                </div>
                <div className={styles.details}>
                    <h2>Invoicing for freelancers</h2>
                    <p>
                        Integrated with Toggl time tracking, the invoicing application is designed specifically for
                        freelancers living outside of their home country.  The app is a simple way to generate invoices based on
                        hourly contracts and an easy, simple dashboard for year-to-date income for multiple countries.
                    </p>
                </div>
            </a>
            <a onClick={() => setProjectSelected("optimizingReact")} className={styles.project}>
                <div className={styles.details}>
                    <h2>Optimizing React</h2>
                    <p>
                        React is one of the largest and well-known Javascript front-end frameworks.  It&quot;s popularity gained
                        through ease of use and modularity can also lead to scaling problems.  It can be easy to
                        write your code into a corner.  There&quot;s a book in the works to help developers navigate React traps
                        and understand the advanced features that can keep large apps quick and efficient.
                    </p>
                </div>
                <div className={styles.thumbnail}>
                    <svg viewBox="0 0 100 100">
                        <path
                            id="page-border"
                            className={styles.bookOutline}
                            d="M 80 5
                               L 20 5
                               Q 10 10 20 15
                               L 75 15
                               L 75 95
                               L 20 95
                               Q 15 95 15 90
                               L 15 10
                               "
                        />
                        <path
                            className={styles.bookBack}
                            d="M 80 5
                               L 80 85
                               L 75 95
                            "
                        />
                        <path
                            className={styles.bookPage}
                            d="M 16 7.5
                               L 78.75 7.5
                               L 78.75 87.5
                            "
                        />
                        <path
                            className={styles.bookPage}
                            d="M 15 10
                               L 77.5 10
                               L 77.5 90
                            "
                        />
                        <path
                            className={styles.bookPage}
                            d="M 16 12.5
                               L 76.25 12.5
                               L 76.25 92.5
                            "
                        />

                        <g className={styles.reactAtom}>
                            <circle className={styles.reactLogoCenter} cx="45" cy="55" r="3" />

                            <g className={styles.reactLogoRings}>
                                <ellipse className={styles.reactLogoRing} cx="45" cy="55" rx="20" ry="5" />
                                <ellipse className={styles.reactLogoRing} cx="45" cy="55" rx="20" ry="5" />
                                <ellipse className={styles.reactLogoRing} cx="45" cy="55" rx="20" ry="5" />
                            </g>
                        </g>
                    </svg>
                </div>
            </a>

            <Modal visible={!!projectSelected && projectSelected in projectsFormInfo} onCloseModal={() => setProjectSelected("")}>
                <ContactForm
                    interestedIn={projectsFormInfo[projectSelected]?.name}
                    formDescription={projectsFormInfo[projectSelected]?.description}
                    messagePrompt="Custom Message"
                    afterSubmit={() => setProjectSelected("")}
                />
            </Modal>
        </>
    );
}

Labs.getLayout = (page: ReactElement) => (
    <Layout>
        <PageLayout>{page}</PageLayout>
    </Layout>
);

export default Labs
