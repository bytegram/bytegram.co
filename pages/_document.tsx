import Document, { Html, Head, Main, NextScript, DocumentContext } from 'next/document';

class BytegramDocument extends Document {
    static async getInitialProps(context: DocumentContext) {
        const initialProps = await Document.getInitialProps(context);
        return { ...initialProps }
    }

    render() {
        return (
            <Html>
                <Head />
                <body>
                    <Main />
                    <NextScript />
                    <div id="modal-root" />
                    <div id="toast-root" />
                </body>
            </Html>
        );
    }
}

export default BytegramDocument;
