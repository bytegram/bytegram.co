import type { MutableRefObject } from 'react'
import ReactGlobe, { GlobeMethods } from 'react-globe.gl'
import type { GlobeProps } from 'react-globe.gl'

const Globe = ({ xref, ...props }: GlobeProps & { xref?: MutableRefObject<GlobeMethods | undefined> | null }) => {
    const forwardedRef: MutableRefObject<GlobeMethods | undefined> | undefined = xref === null ? undefined : xref;
    return <ReactGlobe ref={forwardedRef} {...props} />
};

Globe.displayName = 'Globe'

export default Globe;
