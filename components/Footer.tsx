import BytegramIcon from '../public/bytegram-icon.svg'
import styles from '../styles/Footer.module.css';


export default function Footer() {
    return (
        <footer className={styles.footer}>
            <span className={styles.copyright}>
                &#169; {(new Date()).getFullYear()} Bytegram, LLC
            </span>
            <span className={styles.logo}>
                <BytegramIcon alt="Bytegram Logo" width={72} height={72} />
            </span>
        </footer>
    );
}
