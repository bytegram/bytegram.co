import { useState, useRef, FormEventHandler } from 'react';
import ReCaptcha from '../components/ReCaptcha';
import Toast from '../components/Toast';
import styles from '../styles/ContactForm.module.css'

type ContactFormProperties = {
    interestedIn?: string;
    formDescription?: string;
    messagePrompt?: string;
    afterSubmit?: () => void;
}

const ContactForm = ({ interestedIn = '', formDescription = '', messagePrompt = 'Describe Your Project', afterSubmit = () => {} }: ContactFormProperties) => {
    const [captchaValue, setCaptchaValue] = useState('');
    const [error, setError] = useState('');
    const [submitting, setSubmitting] = useState(false);
    const form = useRef<HTMLFormElement>(null);

    const clearAndCloseForm = () => {
        form.current?.reset();
        afterSubmit();
    }

    const onSubmit: FormEventHandler = (event) => {
        event.preventDefault();
        if (!captchaValue) {
            setError("Please verify the CAPTCHA field");
        } else {
            if (form.current?.reportValidity()) {
                setSubmitting(true);
                fetch('https://us-central1-bytegramweb.cloudfunctions.net/send-form', {
                    method: 'POST',
                    mode: 'cors',
                    cache: 'no-cache',
                    credentials: 'omit',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    body: new URLSearchParams(new FormData(form.current) as any),
                }).then(res => {
                    setSubmitting(false);
                    if (res.status >= 400) {
                        res.json().then(body => setError(body.reason));
                    } else {
                        res.json().then(body => {
                            if (!body.success) {
                                setError(body.reason || 'The contact form is not usable right now.');
                            } else {
                                clearAndCloseForm();
                            }
                        })
                    }
                }).catch(e => {
                    console.error(e);
                    setSubmitting(false);
                    setError("Site is unable to send the form now.  Please try again later.");
                })
            }
        }
    }

    return (
        <form ref={form} onSubmit={onSubmit} encType="application/x-www-form-urlencoded">
            {formDescription && (
                <p>
                    {formDescription}
                </p>
            )}

            <fieldset>
                <input
                    id="from_name"
                    name="from_name"
                    placeholder="Your Name"
                    disabled={submitting}
                    required
                />

                <input
                    id="reply_to"
                    name="reply_to"
                    type="email"
                    placeholder="Your Email"
                    disabled={submitting}
                    required
                />
            </fieldset>

            {interestedIn && <input type="hidden" name="project_type" value={interestedIn} />}
            
            {!interestedIn && (
                <select
                    id="project_type"
                    name="project_type"
                >
                    <option value="">I&quot;m interested in...</option>
                    <option value="Custom Software Build">Custom Software Build</option>
                    <option value="Project Refactor">Project Refactor</option>
                    <option value="Data Integration">Data Integration</option>
                    <option value="Team training / mentoring">Team Training / mentoring</option>
                </select>
            )}

            <textarea
                id="message"
                name="message"
                placeholder={messagePrompt}
                disabled={submitting}
            >
            </textarea>

            <input type="hidden" name="g-recaptcha" disabled={submitting} value={captchaValue} />

            <ReCaptcha
                sitekey="6LeDYoccAAAAALW0qjs8MMQ5iwyzxdx_6nXWsRaK"
                onChange={setCaptchaValue}
            />
            
            {submitting ? (
                <svg className={styles.submitting} width="24" height="24">
                    <circle cx="12" cy="12" r="10" />
                </svg>
            ) : (
                <button type="submit">
                    Send
                </button>
            )}

            <Toast visible={!!error} onClose={() => setError('')} text={error} />

        </form>
    );
}

export default ContactForm;
