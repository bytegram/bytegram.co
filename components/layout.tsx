import Footer from './Footer'
import styles from '../styles/Home.module.css'
import type { FunctionComponent } from 'react'

const Layout: FunctionComponent = ({ children }) => {
    return (
        <div className={styles.container}>
            {children}
            <Footer />
        </div>
    );
}

export default Layout;