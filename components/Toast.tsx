import type { FunctionComponent } from "react";
import { useState, useEffect } from "react"
import ReactDOM from "react-dom"
import styles from "../styles/Toast.module.css";

type ToastProperties = {
    visible: boolean,
    text: string,
    onClose: () => void,
}

const Toast: FunctionComponent<ToastProperties> = ({ text, visible, onClose }) => {
    const [isVisible, setIsVisible] = useState(visible);
    const [children, setChildren] = useState(text);
    
    useEffect(() => {
        console.log(visible, isVisible, text, children)
        if (!visible || !text) {
            setTimeout(() => {
                console.log("waited!", isVisible, children)
                setIsVisible(visible);
                setChildren(text);
            }, 1000);
        } else {
            console.log("immediate!!")
            setIsVisible(visible);
            setChildren(text);
        }
    }, [visible, text]);

    return (isVisible || null) && ReactDOM.createPortal(
        <div className={styles.toast + ' '  + (visible ? styles.showing : styles.hiding)}>
            <div className={styles.toastClose} onClick={onClose}>
                &#10006;
            </div>
            {children}
        </div>,
        document?.getElementById("toast-root") as any
    );
}

export default Toast;
