import Navbar from './Navbar'
import Link from 'next/link'
import BytegramBanner from '../public/bytegram-banner.svg';
import styles from '../styles/Header.module.css';

export default function Header() {
    return (
        <header className={styles.header}>
            <Link href="/">
                <a>
                    <BytegramBanner className={`${styles.logo}`} />
                </a>
            </Link>
            <Navbar />
        </header>
    )
}