import { FunctionComponent } from 'react'
import Header from './Header'
import styles from '../styles/Page.module.css'

const PageLayout: FunctionComponent = ({ children }) => (
    <>
        <Header />
        <div className={styles.container}>
            <main className={styles.main}>{children}</main>
        </div>
    </>
);

export default PageLayout;
