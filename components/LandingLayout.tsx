import type { FunctionComponent } from 'react'
import styles from '../styles/Landing.module.css'

const LandingLayout: FunctionComponent = ({ children }) => (
    <main className={styles.main}>{children}</main>
);

export default LandingLayout;
