import type { FunctionComponent } from 'react'
import type { LinkProps } from 'next/link'
import Link from 'next/link'
import { useRouter } from 'next/router'
import styles from '../styles/Navbar.module.css'

const NavLink: FunctionComponent<LinkProps> = ({ children, ...props }) => {
    const router = useRouter();

    return (
        <Link {...props} >
            <a className={router.asPath === props.href ? styles.active : ''}>
                {children}
            </a>
        </Link>
    );
}

export default NavLink;
