import type { FunctionComponent } from 'react'
import styles from '../styles/Navbar.module.css'
import NavLink from './NavLink'


const Navbar: FunctionComponent = () => {
    return (
        <nav className={styles.navbar}>
            <ul>
                <li>
                    <NavLink href="/about">About</NavLink>
                </li>
                <li>
                    <NavLink href="/portfolio">Portfolio</NavLink>
                </li>
                <li>
                    <NavLink href="/contact">Contact</NavLink>
                </li>
                <li>
                    <NavLink href="/labs">Labs</NavLink>
                </li>
            </ul>
        </nav>
    )
}

export default Navbar;
