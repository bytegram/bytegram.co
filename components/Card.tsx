import type { FunctionComponent, ReactEventHandler } from 'react'
import styles from '../styles/Card.module.css'

type CardProps = {
    title: string
}

const Card: FunctionComponent<CardProps> = ({ children, title }) => {
    const onToggle: ReactEventHandler<HTMLElement> = (event) => {
        event.persist();
        event.preventDefault();
        if (event.currentTarget.style.height === '') {
            const clone = event.currentTarget.cloneNode(true) as HTMLDivElement;
            clone.style.height = 'auto';
            clone.style.visibility = 'hidden';
            event.currentTarget.parentElement?.append(clone);
            const height = clone.getClientRects()[0]?.height;

            event.currentTarget.style.height = height + 'px';
            event.currentTarget.parentElement?.removeChild(clone);
        } else {
            event.currentTarget.style.height = '';
        }
    };

    return (
        <details className={styles.card} onClick={onToggle} open>
            <summary>{title}</summary>
            <div>{children}</div>
        </details>
    )
}

export default Card;
