import { FunctionComponent } from "react"
import ReactDOM from "react-dom"
import styles from "../styles/Modal.module.css"

type ModalProperies = {
    visible: boolean,
    onCloseModal: () => void,
}

const Modal: FunctionComponent<ModalProperies> = ({ children, visible, onCloseModal }) => {

    return (visible || null) && ReactDOM.createPortal(
        <>
            <div className={styles.modalBackdrop} />
            <div className={styles.modal}>
                <div className={styles.modalContent}>
                    <div className={styles.modalClose} onClick={onCloseModal}>
                        &#10006;
                    </div>
                    {children}
                </div>
            </div>
        </>,
        document.getElementById("modal-root") as any
    );
}

export default Modal;
