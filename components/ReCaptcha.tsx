import { useEffect, useRef } from 'react';

type ReCaptchaProperties = {
    id?: string;
    sitekey: string;
    onChange?: (value: string) => void;
}

const ReCaptcha = ({ id = 'default', sitekey, onChange }: ReCaptchaProperties) => {
    const captchaRef = useRef(null);
    useEffect(() => {
        (window as any)[`onLoad_${id}`] = function () {
            console.log('woot!!!!');
            (window as any).grecaptcha.enterprise.render(captchaRef.current, { sitekey, callback: onChange });
        }
        const script = document.createElement('script');
        script.setAttribute('src', `https://www.google.com/recaptcha/enterprise.js?onload=onLoad_${id}&render=explicit`);
        script.setAttribute('async', 'async');
        document.body.appendChild(script);
    }, [id, onChange]);

    return (
        <div id={id} ref={captchaRef}></div>
    );
}

export default ReCaptcha;
